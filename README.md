RRZE Access-Control
===================

WordPress-Plugin
----------------

Es ermöglicht den eingeschränkten Zugriff auf Dateien und Seiten durch benutzerbezogene Funktionen und IP-Adressen.

Einstellungsmenü: Einstellungen › Zugriffsschutz
